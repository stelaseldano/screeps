module.exports =  {
    // takes creep as a parameter
    run: function(creep) {
        // when the creeep is getting energy and is full
        // make it build
        if (creep.memory.building === false && creep.carry.energy === creep.carryCapacity) {
            creep.memory.building = true
        // when the creep is building but has no enery left
        // make it go get energy
        } else if (creep.memory.building && creep.carry.energy === 0) {
            creep.memory.building = false
        // when getting energy and not full
        // continue getting energy
        } else if (creep.memory.building === false) {
            let source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE)

            if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
                creep.moveTo(source)
            }
        // when building and has energy
        // build roads if there are roads to build
        // otherwise change role into upgrader
        } else {
            // find the closest construction
            let construction = creep.pos.findClosestByPath(FIND_MY_CONSTRUCTION_SITES);
            
            // change role to upgrader
            // when there is no construction to work on
            if (!construction) {
                creep.memory.role = 'upgrader'
                creep.memory.working = false
                delete creep.memory.building
            // or build when there is a construcion
            } else if (creep.build(construction) == ERR_NOT_IN_RANGE) {
                creep.moveTo(construction)
            }

        }
    }
}