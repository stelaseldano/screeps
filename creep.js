let helpers = require('helpers')

let improveBody = function(extraEnergy, role) {
    // body base (takes 300 energy)
	let body = [WORK, CARRY, MOVE]

    if (role === 'harvester' || role === 'upgrader') {
       while (extraEnergy > 0) {
            if (Math.floor(extraEnergy / 150) > 0) {
                body.push(WORK, CARRY, MOVE)
                extraEnergy -= 150
            } else if (Math.floor(extraEnergy / 50) > 0 && Math.floor(extraEnergy / 50) % 2 === 0) {
                body.push(MOVE)
                extraEnergy -= 50
            } else if (Math.floor(extraEnergy / 50) > 0 && Math.floor(extraEnergy / 50) % 2 === 1) {
                body.push(CARRY)
                extraEnergy -= 50
            } else {
                body.push(TOUGH)
                extraEnergy -= 10
            }
       }
                   
        return body
    } else if (role === 'builder' || role === 'repairer') {
        while (extraEnergy > 0) {
            if (Math.floor(extraEnergy / 150) > 0) {
                body.push(WORK, CARRY, MOVE)
                extraEnergy -= 150
            } else if (Math.floor(extraEnergy / 50) > 0 && Math.floor(extraEnergy / 50) % 2 === 0) {
                body.push(MOVE)
                extraEnergy -= 50
            } else if (Math.floor(extraEnergy / 50) > 0 && Math.floor(extraEnergy / 50) % 2 === 1) {
                body.push(CARRY)
                extraEnergy -= 50
            } else {
                body.push(TOUGH)
                extraEnergy -= 10
            }
       }

       return body
    } else if (role === 'attacker') {
        let body = []
        extraEnergy += 300

        while (extraEnergy > 0) {
            if (Math.floor(extraEnergy / 130) > 0) {
                body.push(ATTACK, MOVE)
                extraEnergy -= 130
            } else if (Math.floor(extraEnergy / 50) > 0) {
                body.push(MOVE)
                extraEnergy -= 50
            } else {
                body.push(TOUGH)
                extraEnergy -= 10
            }
       }
       return body
    }
}

// creates and buries creeps
module.exports = {
	// takes role as arg
	create: function(role) {
		// get the extra energy (if there are extenstion in the room)
		let extraEnergy = (helpers.getTotalCreeps() === 0) ? 0 : Game.rooms['E11N6'].energyCapacityAvailable - 300

		if (role === 'harvester') {
		    let body = improveBody(extraEnergy, 'harvester')
			let newHarvester = Game.spawns.Spawn1.createCreep(
				body,
				undefined,
				{role: 'harvester', working: false})
			console.log(newHarvester + ' - new harvester is born!')
		} else if (role === 'upgrader') {
			let body = improveBody(extraEnergy, 'upgrader')
			let newUpgrader = Game.spawns.Spawn1.createCreep(
				body,
				undefined,
				{role: 'upgrader', working: false})
			console.log(newUpgrader + ' - new upgrader is born!')
		} else if (role === 'builder') {
			let body = improveBody(extraEnergy, 'builder')
			let newBuilder = Game.spawns.Spawn1.createCreep(
				body,
				undefined,
				{role: 'builder', building: false})
			console.log(newBuilder + ' - new builder is born!')
		} else if (role === 'repairer') {
			let body = improveBody(extraEnergy, 'repairer')
			let newRepairer = Game.spawns.Spawn1.createCreep(
				body,
				undefined,
				{role: 'repairer', repairing: false})
			console.log(newRepairer + ' - new repairer is born!')
		} else if (role === 'attacker') {
		    let body = improveBody(extraEnergy, 'attacker')
			let newAttacker = Game.spawns.Spawn1.createCreep(
				body,
				undefined,
				{role: 'attacker', working: false})
			console.log(newAttacker + ' - new attacker is born!')
		}
	},
	// takes the creep for clearance as arg
	buryDead: function(creep) {
		if (!Game.creeps[creep]) {
			console.log(creep + ' is dead and forgotten')
			delete Memory.creeps[creep]
		}
	}
}