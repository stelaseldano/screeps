// helper functions
module.exports = {
    // returns the total number of living creeps per role
    getTotalCreepsByRole: function(role) {
        let total = 0

        for (let creep in Game.creeps) {
            if (Game.creeps[creep].memory.role === role) {
                total++
            }
        }

        return total
    },
    getTotalCreeps: function() {
        let total = 0;

        for (let creep in Game.creeps) {
            if (Game.creeps.hasOwnProperty(creep)) {
                total++
            }
        }

        return total
    },
    findExtension: function(creep) {
        let extensions = creep.room.find(FIND_MY_STRUCTURES, {filter: (stru) => stru.structureType === STRUCTURE_EXTENSION})
        let extension = extensions.find((ext) => ext.energy < ext.energyCapacity)

        return extension
    }
}