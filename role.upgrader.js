module.exports =  {
    // takes creep as a parameter
    run: function(creep) {
        // when the creeep is getting energy and is full
        // make it repair
        if (creep.memory.working === false && creep.carry.energy === creep.carryCapacity) {
            creep.memory.working = true
        // when the creep is upgrading but has no enery left
        // make it go get energy
        } else if (creep.memory.working && creep.carry.energy === 0) {
            creep.memory.working = false
        // when getting energy and not full
        // continue getting energy
        } else if (creep.memory.working === false) {
            let source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE)

            if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
                creep.moveTo(source)
            }
        // when upgrading and has energy to upgrade
        // upgrade the room controller
        } else {
            if (creep.upgradeController(creep.room.controller) === ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller)
            }
        }
    }
}