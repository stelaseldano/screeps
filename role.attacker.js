module.exports =  {
    // takes creep as a parameter
    run: function(creep) {
        // checks for invaders
        let invaders = creep.room.find(FIND_HOSTILE_CREEPS);

        // if there are invaders
        if (invaders.length > 0) {

            // moves to the invader and attacks it
            if (creep.attack(invaders[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(invaders[0])
            } else {
                console.log('attacking ' + invaders[0])
            }
        // if there are no invaders
        // harvests enrrgy
        } else {
            let tower = creep.room.find(FIND_MY_STRUCTURES, {filter: (str) => str.structureType === STRUCTURE_TOWER})[0]

            creep.moveTo(tower)
        }
    }
}