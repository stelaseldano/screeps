var helpers = require('helpers')

module.exports =  {
    // takes creep as a parameter
    run: function(creep) {
        // when the creeep is getting energy and is full
        // make transfer the energy to the spawn
        if (creep.memory.working === false && creep.carry.energy === creep.carryCapacity) {
            creep.memory.working = true
        // when the creep is transferring energy but has no enery left
        // make it go get energy
        } else if (creep.memory.working && creep.carry.energy === 0) {
            creep.memory.working = false
        // when getting energy and not full
        // continue getting energy
        } else if (creep.memory.working === false) {
            let source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE)

            if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
                creep.moveTo(source)
            }
        // when transferring energy and not depleted
        // continue transferring
        } else {
            let spawn = Game.spawns.Spawn1
            let extension = helpers.findExtension(creep)
            
            if (extension !== undefined) {
                if (creep.transfer(extension, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(extension)
                }
            } else if (creep.transfer(spawn, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                creep.moveTo(spawn)
            }
        }
    }
}