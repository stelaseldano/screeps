let crp = require('creep')
let helpers = require('helpers')
let harvester = require('role.harvester')
let upgrader = require('role.upgrader')
let builder = require('role.builder')
let repairer = require('role.repairer')
let attacker = require('role.attacker')

module.exports.loop = function() {
    let hostiles = Game.rooms['E11N6'].find(FIND_HOSTILE_CREEPS)
    let towers = Game.rooms['E11N6'].find(FIND_MY_STRUCTURES, {filter: (str) => str.structureType === STRUCTURE_TOWER})
    let hasConstructionSites = Game.rooms['E11N6'].find(FIND_MY_CONSTRUCTION_SITES).length === 0 ? false : true
    let hasStructures = Game.rooms['E11N6'].find(FIND_MY_STRUCTURES).length === 0 ? false : true

    // the minimum number per role
    // dynamically sets the minimum depending on the current size of the colony
    const minNumHarvesters = (Math.ceil(helpers.getTotalCreeps() / 3) > 4) ? Math.ceil(helpers.getTotalCreeps() / 3) : 4
    const minNumUpgraders = (Math.ceil(helpers.getTotalCreeps() / 3) > 2) ? Math.ceil(helpers.getTotalCreeps() / 3) : 2
    const minNumBuilders = 4
    const minNumRepairers = 2
    const minNumAttackers = (hostiles.length > 0) ? hostiles.length + 1 : 2
    // the current number per role
    let livingHarvesters = helpers.getTotalCreepsByRole('harvester')
    let livingUpgraders = helpers.getTotalCreepsByRole('upgrader')
    let livingBuilders = helpers.getTotalCreepsByRole('builder')
    let livingRepairers = helpers.getTotalCreepsByRole('repairer')
    let livingAttackers = helpers.getTotalCreepsByRole('attacker')

    // creates new creeps
    //
    // if there are no creeps in the game (killed or starting the game)
    // starts with a harvester
    // otherwise creates creeps depending on the minimum number required per role
    if (helpers.getTotalCreeps() === 0) {
        console.log('creating harvester')
        crp.create('harvester')
    } else {
        // creates creeps when the spawn is full
        if (Game.spawns.Spawn1.energy === Game.spawns.Spawn1.energyCapacity) {
            if (livingHarvesters < minNumHarvesters) {
                crp.create('harvester')
            } else if (livingAttackers < minNumAttackers) {
                crp.create('attacker')
            } else if (livingUpgraders < minNumUpgraders) {
                crp.create('upgrader')
            } else if (hasStructures && livingRepairers < minNumRepairers) {
                crp.create('repairer')
            } else if (hasConstructionSites && livingBuilders < minNumBuilders) {
                crp.create('builder')
            } else {
                crp.create('builder')
            }
        }
    }
    
    // clears the memory from the dead creeps
    for (let creep in Memory.creeps) {
        crp.buryDead(creep)
    }

    // runs the creeps
    for (let name in Game.creeps) {
        let creep = Game.creeps[name]

        if (creep.memory.role === 'harvester') {
            harvester.run(creep)
        } else if (creep.memory.role === 'upgrader') {
            upgrader.run(creep)
        } else if (creep.memory.role === 'builder') {
            builder.run(creep)
        } else if (creep.memory.role === 'repairer') {
            repairer.run(creep)
        } else if (creep.memory.role === 'attacker') {
            attacker.run(creep)
        } else {
            console.log('no such creep role')
        }
    }
    
    // if there are hostiles detected, activates the tower
    if (hostiles.length > 0) {
        towers.forEach((tower) => {
            tower.attack(hostiles[0])
        })
    }
}