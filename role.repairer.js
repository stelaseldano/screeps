module.exports =  {
    // takes creep as a parameter
    run: function(creep) {
        // when the creeep is getting energy and is full
        // make it repair
        if (creep.memory.repairing === false && creep.carry.energy === creep.carryCapacity) {
            creep.memory.repairing = true
        // when the creep is repairing but has no enery left
        // make it go get energy
        } else if (creep.memory.repairing && creep.carry.energy === 0) {
            creep.memory.repairing = false
        // when getting energy and not full
        // continue getting energy
        } else if (creep.memory.repairing === false) {
            let source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE)

            if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
                creep.moveTo(source)
            }
        // when repairing and has energy to repair
        // repair roads if there are such
        // otherwise change role into upgrader
        } else {
            // gets all roads
            let roads = creep.room.find(FIND_STRUCTURES, {
                filter: (str) => str.structureType === STRUCTURE_ROAD
            })
            // gets all ramparts
            let ramparts = creep.room.find(FIND_STRUCTURES, {
                filter: (str) => str.structureType === STRUCTURE_RAMPART
            })
            // takes the first road from roads that will decay in 1000 hits
            let road = roads.find((r) => r.hits < 1000)

            // repairs the decaying road
            if (road) {
                if (creep.repair(road) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(road)
                }
            // or changes the role to upgrader
            } else {
                console.log(creep + ' is now an upgrader')
                creep.memory.role = 'upgrader'
                creep.memory.working = false
                delete creep.memory.repairing
            }
        }
    }
}